function invert(testObject){
    let newObj = {};
    if(testObject == undefined || typeof(testObject)!= 'object' || testObject.constructor != Object){
        return newObj;
    }
    for(index in testObject){
        newObj[testObject[index]] = index;
    }
    return newObj;
}

module.exports = invert;