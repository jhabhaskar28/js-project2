function pairs(testObject){
    let pairArray = [];
    if(typeof(testObject) != 'object' || testObject == undefined || testObject.constructor != Object){
        return pairArray;
    }
    for(index in testObject){
        let newPair = [];
        newPair.push(index);
        newPair.push(testObject[index]);
        pairArray.push(newPair);
    }
    return pairArray;
}

module.exports = pairs;
