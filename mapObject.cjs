function mapObject(testObject,callBack){
    if(testObject == undefined || testObject.constructor != Object || typeof(testObject) != 'object'){
        return {};
    } else if(callBack == undefined || callBack.constructor != Function || typeof(callBack) != 'function'){
        return testObject;
    } else {
        for(index in testObject){
            testObject[index] = callBack(testObject[index],index);
        }
        return testObject;
    }
}
module.exports = mapObject;