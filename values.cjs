function values(testObject){
    let valueArray = [];
    if(typeof(testObject) != 'object' || testObject == undefined || testObject.constructor != Object) {
        return valueArray;
    }
    for(index in testObject){
        valueArray.push(testObject[index]);
    }
    return valueArray;
}

module.exports = values;