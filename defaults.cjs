function defaults(testObject,defaultObj){
    if(typeof(testObject)!= 'object' || testObject == undefined || testObject.constructor != Object){
        return {};
    } else if (defaultObj == undefined || defaultObj.constructor != Object || typeof(defaultObj)!= 'object'){
        return testObject;
    } else {
        for(index in defaultObj){
            if(testObject[index] == undefined){
                testObject[index] = defaultObj[index];
            }
        }
        return testObject;
    }
}

module.exports = defaults;