let mapObject = require('../mapObject.cjs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// const testObject = {start: 5, end: 12};

function callBack(value, key){
    return value + 5;
}

// const result = maps(testObject,callBack);
// console.log(result);

// const result = maps(callBack);
// console.log(result);

// const result = maps();
// console.log(result);

// const result = maps({},callBack);
// console.log(result);


const result = mapObject(testObject);
console.log(result);