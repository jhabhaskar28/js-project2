function keys(testObject){
    let keyArray = [];
    if(typeof(testObject) != 'object' || testObject == undefined || testObject.constructor != Object){
        return keyArray;
    }
    for(index in testObject){
        keyArray.push(index);
    }
    return keyArray;
}

module.exports = keys;